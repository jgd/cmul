#ifndef __H264Encoder
#define __H264Encoder

#include "frame.h"

// FFMPEG
extern "C" {
#include <libavcodec/avcodec.h>
}

class H264Encoder {
 public:
  H264Encoder();
  ~H264Encoder();
  bool init(int width, int height, int fps, int gop, int bperiod, int rate, int maxPayload);
  bool encode(Frame *f, uint8_t **output, uint32_t * out_size, int64_t *out_pts);
  void free_output();
  void close();
 private:
  AVCodec *codec;
  AVCodecContext *context;
  AVFrame *frame;
  AVPacket pkt;
  int64_t pts;
};

#endif
