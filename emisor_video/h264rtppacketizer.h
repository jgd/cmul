#ifndef __H264RTPPacketizer
#define __H264RTPPacketizer

#include <stdio.h>
#include <stdint.h>
#include <list>
#include "rtpsender.h"

#define MTU 1472
#define RTPHEADER 12
#define MAXPAYLOAD MTU-RTPHEADER
#define RTPCLOCK 90000

using namespace std;

class H264RTPPacketizer {
 public:
  bool init(uint16_t fps, RTPSender *sender);
  bool send(uint8_t *buffer, uint32_t size, int64_t pts);
  void close() { };
  uint16_t getClock();
  uint16_t getMaxPayload();
 private:
  uint32_t period;
  list<uint32_t> nalu_start;
  list<uint16_t> nalu_size;
  uint8_t payload[MAXPAYLOAD];
  uint32_t payload_size;
  RTPSender* sender;
  FILE *fout;
};
#endif
