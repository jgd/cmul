#include "h264encoder.h"

extern "C" {
#include <libavutil/opt.h>
#include <libavutil/imgutils.h>
}

H264Encoder::H264Encoder() {
  // Registrar codecs
  avcodec_register_all();
  // Obtener referencia al codec H264
  codec = avcodec_find_encoder(AV_CODEC_ID_H264);
  if (!codec) {
    fprintf(stderr,"Codec H264 no encontrado\n");
    exit(1);
  }
  frame = NULL;
  context = NULL;
}

H264Encoder::~H264Encoder() {
  close();
}

void H264Encoder::close() {
  if (context) {
    avcodec_close(context);
    av_free(context);
    avcodec_free_frame(&frame);
    context = NULL;
    frame = NULL;
  }
}

bool H264Encoder::init(int width, int height, int fps, int gop, int bperiod, int rate, int maxPayload) {
  context = avcodec_alloc_context3(codec);
  context->width = width;
  context->height = height;
  context->pix_fmt = AV_PIX_FMT_YUV420P;
  context->time_base = (AVRational){1,fps};
  context->gop_size = gop;
  context->keyint_min = gop;
  context->max_b_frames = bperiod;
  context->rc_max_rate = rate;
  context->rc_buffer_size = rate/4;

  av_opt_set(context->priv_data, "preset", "ultrafast", 0);
  char param_max_slice[128];
  sprintf(param_max_slice,"slice-max-size=%d",maxPayload);
  av_opt_set(context->priv_data, "x264opts", param_max_slice, 0);

  if (avcodec_open2(context, codec, NULL) < 0) {
    fprintf(stderr, "No se puede abrir el compresor!\n");
    return false;
  }

  frame = avcodec_alloc_frame();
  if (!frame) {
    fprintf(stderr, "No se puede reservar un frame!\n");
    return false;
  }
  frame->format = context->pix_fmt;
  frame->width = context->width;
  frame->height = context->height;
  int ret = av_image_alloc(frame->data, frame->linesize, context->width, context->height,
			   context->pix_fmt, 1);
  if (ret < 0) {
    fprintf(stderr, "No se pueden reservar los buffers del frame!\n");
    return false;
  }

  av_init_packet(&pkt);
  pkt.data = 0;
  pkt.size = 0;

  pts = 0;

  return true;
}

bool H264Encoder::encode(Frame *f, uint8_t **output, uint32_t * out_size, int64_t *out_pts) {
  FrameYUV *fyuv = (FrameYUV *)f;
  AVFrame *faux = frame;
  if (fyuv) {
    memcpy(frame->data[0],fyuv->Y(),fyuv->sizeY);
    memcpy(frame->data[1],fyuv->U(),fyuv->sizeUV);
    memcpy(frame->data[2],fyuv->V(),fyuv->sizeUV);
    frame->pts = pts++;
  }
  else {
    faux = 0;
  }

  int got_output,ret;
  ret = avcodec_encode_video2(context,&pkt,faux,&got_output);
  if (ret < 0) {
    fprintf(stderr,"Error codificando frame!\n");
    return false;
  }
  if (got_output) {
    *output = pkt.data;
    *out_size = pkt.size;
    *out_pts = pkt.pts;
  }
  else {
    *output = 0;
    *out_size = 0;
    *out_pts = 0;
  }
  return true;
}

void H264Encoder::free_output() {
  av_free_packet(&pkt);
  pkt.data = NULL;
  pkt.size = 0;
}
