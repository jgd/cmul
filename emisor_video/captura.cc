#include "camara.h"
#include "h264encoder.h"
#include "display.h"
#include <unistd.h>
#include "h264rtppacketizer.h"
#include "rtpsender.h"

#define INFOHEIGHT 50

int main(int argc, char *argv[]) {

  if (argc != 11) {
    printf("uso: %s width height fps device gop bperiod bitrate pLocal ipDest pDest\n",argv[0]);
    exit(1);
  }
   
  uint32_t width = atoi(argv[1]);
  uint32_t height = atoi(argv[2]);
  uint32_t fps = atoi(argv[3]);
  uint32_t gop = atoi(argv[5]);
  uint32_t bperiod = atoi(argv[6]);
  uint32_t bitrate = atoi(argv[7]);
  uint16_t pLocal = atoi(argv[8]);
  const char *ipDest = argv[9];
  uint16_t pDest = atoi(argv[10]);


  int16_t res;

  CamaraYUV camara;
  res = camara.open(argv[4]);
  if (res <0) exit(1);
   
  res = camara.init(width,height);
  if (res <0) exit(1);

  DisplayYUV *display = DisplayYUV::init(width,height+INFOHEIGHT);
  Rect rOverlay;
  rOverlay.x = 0;
  rOverlay.y = 0;
  rOverlay.w = width;
  rOverlay.h = height;
  OverlayYUV overlay(display, &rOverlay);

  display->caption((char*)"Comunicaciones Multimedia");
  
  FrameYUV *f = 0;
  uint64_t intervalo = 1000000L / fps; // Intervalo entre frames (microseg)   
  uint64_t num_frames = 0;
  uint64_t time_inicio = display->now();
  uint64_t time_fin;
  uint64_t captura_siguiente = time_inicio + intervalo;
  uint64_t ahora;
  uint64_t espera;
  char msg_config[256];
  char msg_frame[256];
   
  sprintf(msg_config,"width x height = %u x %u; fps = %u",width,height,fps);
  display->print(10,height+5,msg_config);

  H264RTPPacketizer pack;
  H264Encoder enc;
  if (!enc.init(width,height,fps,gop,bperiod,bitrate,pack.getMaxPayload())) {
    fprintf(stderr, "Error inicializando codec H264\n");
    exit(1);
  }

  RTPSender rtp;

  // Puerto local: pLocal
  // IP Destino: ipDest
  // Puerdo destino: pDest
  // Reloj RTP : pack.getClock()
  // Payload Type: 100
  if (!rtp.init(pLocal,ipDest,pDest,pack.getClock(),bitrate,100)) {
    fprintf(stderr, "Error inicializando emisor rtp\n");
    exit(1);
  }
 
  if (!pack.init(fps,&rtp)) {
    fprintf(stderr, "Error inicializando empaquetador H264/RTP\n");
    exit(1);
  }

  uint8_t *frame_comprimido;
  uint32_t frame_size;
  int64_t pts;

  while(1) {   
    if (camara.grab((Frame **)&f) < 0) { 
      fprintf(stderr, "Error capturando\n"); exit(1);
    }

    if (overlay.display(f) < 0) { 
      fprintf(stderr, "Error visualizando\n"); exit(1);
    }
   
    // Comprimo el frame
    if (!enc.encode(f,&frame_comprimido,&frame_size,&pts)) {
      fprintf(stderr, "Error comprimiendo\n"); exit(1);
    }

    if (frame_size > 0) {
      // Envio el frame comprimido al empaquetador
      if (!pack.send(frame_comprimido,frame_size,pts)) {
	fprintf(stderr,"Error empaquetando frame\n"); exit(1);
      }
    }

    // Libero el frame comprimido
    enc.free_output();
 
    if (display->poll_quit()) break;
    ahora = display->now();
    if (captura_siguiente > ahora) {         
      espera = captura_siguiente - ahora;
      usleep(espera); 
    }      
    captura_siguiente += intervalo;
    num_frames += 1;
    
    // Update info display
    sprintf(msg_frame,"frame: %llu",num_frames);
    display->print(10,height+25,msg_frame);
  }

  while (1) {
    // FLUSH
    if (!enc.encode(NULL,&frame_comprimido,&frame_size,&pts)) {
      fprintf(stderr, "Error comprimiendo\n"); exit(1);
    }

    if (frame_size > 0) {
      if (!pack.send(frame_comprimido,frame_size,pts)) {
	fprintf(stderr,"Error empaquetando frame\n"); exit(1);
      }
    }
    else break;

    // Libero el frame comprimido
    enc.free_output();
    num_frames ++;
  }

  time_fin = display->now();
  camara.close();
  DisplayYUV::close();
  enc.close();
  pack.close();
  rtp.close();
  if (f != 0) delete f; 
      
  double time_total = ((double)(time_fin-time_inicio))/1000000.0;
  double fps_real = (double)num_frames / time_total;
  printf("\nFrames total: %lld, Tiempo total: %.3f seg, Media: %.3f fps\n", 
         num_frames, time_total, fps_real);
   
}
