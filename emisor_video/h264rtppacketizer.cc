#include "h264rtppacketizer.h"
#include <stdio.h>
#include <arpa/inet.h>
#include <string.h>

bool H264RTPPacketizer::init(uint16_t fps, RTPSender *sender) {
  this->sender = sender;
  this->period = RTPCLOCK/fps;
  return true;
}

uint16_t H264RTPPacketizer::getClock() {
  return (uint16_t)RTPCLOCK;
}

uint16_t H264RTPPacketizer::getMaxPayload() {
  return MAXPAYLOAD - 3;
}

bool H264RTPPacketizer::send(uint8_t *buffer, uint32_t size, int64_t pts) {
  uint32_t pos = 0;
  uint32_t prev_start = 0;
  uint16_t ceros = 0;
  uint16_t nalidx = 0;

  while (pos < size) {
    switch(buffer[pos++]) {
    case 0x00:
      ceros++;
      break;
    case 0x01:
      if (ceros >= 2) {
	// Encontrado comienzo de NALU
	nalu_start.push_back(pos);
	if (prev_start != 0) {
	  nalu_size.push_back(pos - prev_start - (ceros+1) );
	}
	prev_start = pos;
      }
    default:
      ceros = 0;
    }
  }
  nalu_size.push_back(pos - prev_start);
  
  uint16_t size_aux;
  pos = 1;
  uint8_t nri = 0;
  payload_size = 1;

  while (!nalu_start.empty()) {
    if (payload_size + 2 + nalu_size.front() <= MAXPAYLOAD) {
      // La siguiente NALU cabe en el paquete STAP actual
      size_aux = htons(nalu_size.front());
      memcpy(&(payload[pos]),&size_aux,2);
      pos += 2;
      memcpy(&(payload[pos]),&(buffer[nalu_start.front()]),nalu_size.front());
      pos += nalu_size.front();
      if (nri < (buffer[nalu_start.front()] & 0x60)) {
	nri = buffer[nalu_start.front()] & 0x60;
      }
      payload_size += nalu_size.front() + 2;
      nalu_start.pop_front();
      nalu_size.pop_front();
    }
    else {
      // Mandar el paquete STAP y preparar el siguiente
      payload[0] = 24;
      payload[0] |= nri;
      // Enviar el paquete RTP.
      sender->send(payload,payload_size,pts*period,false);
      // Reset del paquete RTP para las siguientes NALUs
      payload_size = 1;
      pos = 1;
    }
  }

  // Mandar el ultimo paquete STAP
  payload[0] = 24;
  payload[0] |= nri;
  sender->send(payload,payload_size,pts*period,true);
  return true;
}


