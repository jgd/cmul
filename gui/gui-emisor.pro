#-------------------------------------------------
#
# Project created by QtCreator 2013-04-18T19:51:11
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = gui
TEMPLATE = app

INCLUDEPATH = ../emisor_video
DEPENDPATH = ../emisor_video

SOURCES += main.cpp\
		mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

#LIBS += -L$$PWD/../emisor_video -lemisor_lib
#PRE_TARGETDEPS += $$PWD/../emisor_video/libemisor_lib.a

#INCLUDEPATH += $$PWD/../emisor_video
#DEPENDPATH += $$PWD/../emisor_video



LIBS += -lSDL -lSDL_image -lSDL_ttf -ljrtp -ljthread -lturbojpeg -pthread -L/usr/local/lib -lavcodec -ldl -lva -lXfixes -lXext -lX11 -ljack -lasound -lSDL -lx264 -lvpx -lvorbisenc -lvorbis -ltheoraenc -ltheoradec -logg -lspeex -lrtmp -lgnutls -lopencore-amrwb -lopencore-amrnb -lmp3lame -lfdk-aac -lfaac -lass -lz -lrt -lavutil -lm

QMAKE_CXXFLAGS += $$system(sdl-config --cflags) -D__STDC_CONSTANT_MACROS -I/usr/local/include



