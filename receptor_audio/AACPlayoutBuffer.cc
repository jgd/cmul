#include "AACPlayoutBuffer.h"

using namespace std;

AACPlayoutBuffer::AACPlayoutBuffer(){
    bufferSize = 0;
    cola = new ColaAudio;
    mutex = SDL_CreateMutex();
    estaLlenandose = true;
}

AACPlayoutBuffer::~AACPlayoutBuffer(){
    delete cola;
    SDL_DestroyMutex(mutex);
}

bool AACPlayoutBuffer::insertFrame(uint8_t *buffer, uint32_t size, int64_t pts, uint32_t seqNum)
{
    // Bloquear el mutex
    SDL_mutexP(mutex);

    if(bufferSize + size <= getMaxBufferSize() && pts > ptsUltimoFrameReproducido) {

            ptsUltimoFrameInsertado = pts;

            AACFrame aacFrame;
            aacFrame.buffer = buffer;
            aacFrame.size = size;
            aacFrame.pts = pts;

            // Introducir frame en la cola en su lugar correcto
            cola->push(aacFrame);

            // Aumenta la ocupacion del buffer
            bufferSize += size;

            printf("Llenando: buffer %d\%\n", bufferSize *100 / getMaxBufferSize());

            // Liberar el mutex
            SDL_mutexV(mutex);
            return true;
    }

    // Si hemos llegado aqui, o bien es un frame antiguo o bien no cabe
	if(pts > ptsUltimoFrameReproducido) {
		printf("Un frame en fuera de orden\n");
	}
    // Liberar el mutex y salir
    SDL_mutexV(mutex);
    return false;
}


bool AACPlayoutBuffer::getFrame(uint8_t **buffer, uint32_t *size, int64_t *pts)
{
    // Bloquear el mutex
    SDL_mutexP(mutex);

    // TODO : Añadir funcionalidad para control de buffer vacio o sin superar el minimo
    if (cola->empty())
    {
        // Esperar a que se llene hasta el minimo
        // Liberar el mutex
        estaLlenandose = true;
        SDL_mutexV(mutex);
        return false;
    }
    else
    {
        // Comprueba que no este llenandose
        if (estaLlenandose)
        {
            if(bufferSize > getBufferMinLevel() ) {
                estaLlenandose = false;
            }
            // Si no se ha llenado lo suficiente hay que esperar
            // Liberar el mutex
            SDL_mutexV(mutex);
            return false;

        }
        else
        {

            // Devolver el primer frame de la cola
            AACFrame frame = (AACFrame) cola->top();
            *size = frame.size;
            *pts = frame.pts;
            *buffer = frame.buffer;

            // Sacar frame de la cola
            cola->pop();

            ptsUltimoFrameReproducido = *pts;

            bufferSize -= *size;

           
            // Liberar el mutex
            SDL_mutexV(mutex);
            return true;
        }
    }
}



void AACPlayoutBuffer::getTimeInBuffer(int64_t *pts)
{
    // Bloquear el mutex
    SDL_mutexP(mutex);

    *pts = ptsUltimoFrameInsertado - cola->top().pts;

//    printf("ptsUltimoFrame (%d) - cola.top (%d) = %d ", ptsUltimoFrameInsertado, cola->top().pts, *pts);
    // Liberar el mutex
    SDL_mutexV(mutex);
}

double AACPlayoutBuffer::mostrarEstado() {

	double bufferSizeD = (double) bufferSize;
	double maxBuffer = (double) getMaxBufferSize();

	return  bufferSizeD *100.0 / maxBuffer;
	

}
