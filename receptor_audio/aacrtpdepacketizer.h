#ifndef __AACRTPDEPACK
#define __AACRTPDEPACK

#include "rtpreceiver.h"
#include <list>

using namespace std;

typedef struct AACFrame {
  uint8_t *buffer;
  uint32_t size;
  int64_t pts;
  friend bool operator<(const AACFrame &a, const AACFrame &b);
} AACFrame;



class AACRTPDepacketizer {
 public:
  bool init(uint32_t sample_rate, uint32_t samples_per_frame, RTPReceiver *receiver,uint32_t firstTS = ~0);
  bool receive(list<AACFrame> *frames);
  void close();
  void parar();
  uint32_t getClock();
 private:
  uint32_t sample_rate;
  uint32_t period;
  RTPReceiver *receiver;
  uint32_t firstTS;
  uint32_t seqNum;
  bool stop;
  bool first_packet;
};

#endif
