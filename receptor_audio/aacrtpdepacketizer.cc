#include "aacrtpdepacketizer.h"
#include <unistd.h>

bool AACRTPDepacketizer::init(uint32_t sample_rate, uint32_t samples_per_frame,
			      RTPReceiver *receiver, uint32_t firstTS) {
  this->sample_rate = sample_rate;
  this->period = samples_per_frame;
  this->receiver = receiver;
  this->firstTS = firstTS;
  first_packet = true;
  stop = false;
  seqNum = 0;
  return true;
}

bool operator<(const AACFrame &a, const AACFrame &b) { return a.pts < b.pts; }

bool AACRTPDepacketizer::receive(list<AACFrame> *frames) {
  stop = false;
  while(!stop) {
    if (!receiver->getPayloadData()) {
      // No hay datos
      if (receiver->isClosed()) {
	// El otro extremo se ha cerrado
	return false;
      }
      else {
	// Intento leer un paquete
	if (receiver->receive()) {
	  // Tengo un paquete disponible
	  if (first_packet) {
	    // Es el primer paquete.
	    // Guardo firstTS si hace falta
	    if (firstTS == ~0) firstTS = receiver->getTimestamp();
	    seqNum = receiver->getSeqNum();
	  }
	  else {
	    if (seqNum + 1 != receiver->getSeqNum()) {
	      printf("Perdidos paquetes desde %d hasta %d\n",seqNum+1,receiver->getSeqNum());
	    } 
	    seqNum = receiver->getSeqNum();
	  }
	  // Salgo del bucle porque ya tengo un paquete
	  break; 
	}
	else {
	  // No hay paquetes
	  usleep(1);
	}
      }
    }
  }
  if (!receiver->getPayloadData()) return false;

  // Tengo un paquete, lo proceso.
  const uint8_t *payload = receiver->getPayloadData();
  uint16_t headers_length;
  memcpy(&headers_length,payload,2); // Siempre tiene 2 bytes headers_length
  headers_length = ntohs(headers_length);
  uint16_t frame_count = headers_length / 16;
  printf("Frame count: %d\n",frame_count);

  uint32_t pos = 2;
  uint16_t size;
  AACFrame aux;
  int64_t pts_0 = (receiver->getTimestamp() - firstTS) / period; 
  const uint8_t *payload_frames = payload + 2 + frame_count * 2;
  for (int i = 0; i < frame_count; i++) {
    size = payload[pos] << 5;
    size += (payload[pos+1] >> 3);
    pos += 2;
    //printf("Size frame(%d) = %d\n",i,size);
    aux.size = size;
    aux.pts = pts_0 + i;
    aux.buffer = (uint8_t *)malloc(size);
    memcpy(aux.buffer,payload_frames,size);
    payload_frames += size;
    frames->push_back(aux);
  }

  receiver->freePacket();
  return true;
}

void AACRTPDepacketizer::close() {
  parar();
} 

void AACRTPDepacketizer::parar() {
  stop = true;
}

uint32_t AACRTPDepacketizer::getClock() {
  return sample_rate;
}
