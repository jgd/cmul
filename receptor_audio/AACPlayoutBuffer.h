#ifndef AACPLAYOUTBUFFER
#define AACPLAYOUTBUFFER

#include <queue>
#include <list>
#include "PlayoutBuffer.h"
#include "aacrtpdepacketizer.h"
#include <SDL/SDL_mutex.h>

extern "C" {
#include <libavcodec/avcodec.h>
}

class AACPlayoutBuffer: public PlayoutBuffer
{
public:
    AACPlayoutBuffer();
    ~AACPlayoutBuffer();
    bool insertFrame(uint8_t *buffer, uint32_t size, int64_t pts, uint32_t seqNum);
    bool getFrame(uint8_t **buffer, uint32_t *size, int64_t *pts);
    void getTimeInBuffer(int64_t *pts);

    double mostrarEstado();
protected:

    uint32_t getMaxBufferSize() { return 20000; }
    uint32_t getBufferMinLevel() { return getMaxBufferSize() * 3 / 4; }

    typedef std::priority_queue <AACFrame> ColaAudio;
    ColaAudio* cola;

private:
    SDL_mutex* mutex;
    bool estaLlenandose;
};

#endif
