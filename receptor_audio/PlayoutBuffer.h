#ifndef __PLAYOUTBUFFER
#define __PLAYOUTBUFFER

#include <queue>
#include <list>

extern "C" {
#include <libavcodec/avcodec.h>
}

class PlayoutBuffer
{
public:
    bool insertFrame(uint8_t *buffer, uint32_t size, int64_t pts, uint32_t seqNum);
    bool getFrame(uint8_t **buffer, uint32_t *size, int64_t *pts);
    void getTimeInBuffer(int64_t *pts);


protected:
    int64_t ptsUltimoFrameReproducido, ptsUltimoFrameInsertado;
    uint32_t bufferSize;
    virtual uint32_t getMaxBufferSize() = 0;
    virtual uint32_t getBufferMinLevel() = 0;
};

#endif
