#include "aacdecoder.h"

AACDecoder::AACDecoder() {
  avcodec_register_all();
  codec = avcodec_find_decoder(AV_CODEC_ID_AAC);
  if (!codec) {
    fprintf(stderr,"No se puede encontrar el descompresor AAC!\n");
    exit(1);
  }
  ctx = 0;
  frame = 0;
}

bool AACDecoder::init() {
  ctx = avcodec_alloc_context3(codec);
  if (avcodec_open2(ctx, codec, NULL) < 0) {
    fprintf(stderr, "No se puede abrir el descompresor!\n");
    exit(1);
  }
  av_init_packet(&pkt);
  frame = avcodec_alloc_frame();
  if (!frame) {
    fprintf(stderr, "No se puede reservar un frame!\n");
    exit(1);
  }
  return true;
}

AACDecoder::~AACDecoder() {
  close();
}

void AACDecoder::close() {
  if (ctx) {
    avcodec_close(ctx);
    av_free(ctx);
    ctx = 0;
  }
  if (frame) {
    avcodec_free_frame(&frame);
    frame = 0;
  }
}

bool AACDecoder::decode(uint8_t *in_buffer, uint32_t in_size, int64_t in_pts,
		       uint8_t **out_buffer, uint32_t *out_size, int64_t *out_pts) {

  // Rellenar la instancia de AVPacket con datos del frame
  uint32_t aux_buffer_size = in_size + FF_INPUT_BUFFER_PADDING_SIZE;
  uint8_t *aux_buffer = (uint8_t*)malloc(aux_buffer_size);
  memset(aux_buffer,0,aux_buffer_size);
  memcpy(aux_buffer,in_buffer,in_size);
  av_init_packet(&pkt);
  pkt.data = aux_buffer;
  pkt.size = aux_buffer_size;
  pkt.pts = in_pts;

  int ret,got_frame;
  ret = avcodec_decode_audio4(ctx, frame, &got_frame, &pkt);
  if (ret < 0) {
    fprintf(stderr, "Error descomprimiendo frame de audio!\n");
    free(aux_buffer);
    return false;
  }
  // Procesar datos del frame
  if (got_frame) {
    *out_size = ctx->channels * frame->nb_samples * sizeof (uint16_t);
    *out_buffer = (uint8_t *)malloc(*out_size);
    int16_t *aux_out_buffer = (int16_t*)*out_buffer;
    *out_pts = frame->pkt_pts;
    // Conversion de FLTP a S16
    float *channel0 = (float*)frame->extended_data[0];
    float *channel1 = (float*)frame->extended_data[1];
    float sample0,sample1;
    for (int i = 0; i < frame->nb_samples; i++) {
      sample0 = *channel0++;
      sample1 = *channel1++;
      if (sample0 < -1.0f) sample0 = -1.0f;
      else if (sample0 > 1.0f) sample0 = 1.0f;
      if (sample1 < -1.0f) sample1 = -1.0f;
      else if (sample1 > 1.0f) sample1 = 1.0f;
      aux_out_buffer[2*i] = (int16_t)(sample0 * 32767.0f);
      aux_out_buffer[2*i+1]= (int16_t)(sample1 * 32767.0f);
    }
  }
  else {
    *out_size = 0;
    *out_buffer = 0;
    *out_pts = 0;
  }

  free(aux_buffer);
  return true;
}

