#ifndef __AACDECODER_H
#define __AACDECODER_H

extern "C" {
#include <libavcodec/avcodec.h>
}

class AACDecoder {
 public:
  AACDecoder();
  ~AACDecoder();
  bool init();
  bool decode(uint8_t *in_buffer, uint32_t in_size, int64_t in_pts,
	      uint8_t **out_buffer, uint32_t *out_size, int64_t *out_pts);
  void close();
 private:
  AVCodec *codec;
  AVCodecContext *ctx;
  AVFrame *frame;
  AVPacket pkt;
};

#endif
