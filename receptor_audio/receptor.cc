#include "rtpreceiver.h"
#include "aacrtpdepacketizer.h"
#include "aacdecoder.h"
#include "audio_out.h"
#include "AACPlayoutBuffer.h"
#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <sndfile.h>
#include <SDL/SDL_thread.h>

bool stop;
RTPReceiver receiver;
AACRTPDepacketizer depack;
AACDecoder decoder;
AudioOut aOut;
AACPlayoutBuffer playoutBuffer;

void sighand(int sig)
{
    stop = true;
    depack.parar();
}

int consumidor(void *data);
int estadisticas(void *data);

int main(int argc, char *argv[])
{
    if (argc != 4)
    {
        fprintf(stderr, "Uso: %s puerto sample_rate samples_per_frame\n", argv[0]);
        exit(1);
    }

    bool ret = receiver.init(atoi(argv[1]), atoi(argv[2]));
    if (!ret)
    {
        printf("Error inicializando receptor\n");
        exit(1);
    }
    ret = depack.init(atoi(argv[2]), atoi(argv[3]), &receiver);
    if (!ret)
    {
        printf("Error inicializando desempaquetador\n");
        exit(1);
    }



    ret = aOut.init();
    if (ret < 0)
    {
        printf("Error inicializando AudioOut\n");
        exit(1);
    }

    SNDFILE *fwav;
    SF_INFO sf_info;
    sf_info.samplerate = atoi(argv[2]);
    sf_info.channels = 2;
    sf_info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;

    fwav = sf_open("out.wav", SFM_WRITE, &sf_info);
    if (fwav == NULL)
    {
        fprintf(stderr, "Error abriendo out.wav\n");
        exit(1);
    }

    stop = false;
    signal(SIGINT, sighand);
    signal(SIGQUIT, sighand);
    list<AACFrame> frames;

//    FILE *fout = fopen("out.aac", "wb");

    // Crear hilo para consumidor pasando buffer
    SDL_Thread *hilo = SDL_CreateThread(consumidor, NULL);
	SDL_Thread *hiloStats = SDL_CreateThread(estadisticas, NULL);

    // Comenzar a recibir
    while (!stop)
    {
        if (depack.receive(&frames))
        {
            // Grabar a fichero
            while (frames.size() != 0)
            {
                AACFrame aux = frames.front();
                frames.pop_front();
//                fwrite(aux.buffer, aux.size, 1, fout);
                // Meter frame al buffer

                if (playoutBuffer.insertFrame(aux.buffer, aux.size, aux.pts, 0/*receiver.getSeqNum()*/)) {
                    // He podido meter el frame en el buffer
                } else {
                    printf("Se ha perdido un frame!\n");
                }

                // Mostrar estado del buffer
                //playoutBuffer.mostrarEstado();

            }
        }
        else break;
    }

    // Si vamos a terminar hay que esperar a que el hilo se cierre
    SDL_WaitThread(hilo, NULL);
    SDL_WaitThread(hiloStats, NULL);

//    fclose(fout);
    sf_close(fwav);
    receiver.close();
    depack.close();
//    decoder.close();
    aOut.close();
}

uint64_t getNow() {
	struct timeval tiempo;
	gettimeofday(&tiempo, NULL);
	
	uint64_t resultado = (tiempo.tv_sec * 1000000 + tiempo.tv_usec);
	return resultado;
}

int estadisticas(void *data) {
	FILE *salida = fopen("salida.dat","w");
	char estadistica[256];
	while(!stop){
		sprintf(estadistica,"%llu\t%f\n", getNow(), playoutBuffer.mostrarEstado());
		fputs(estadistica,salida);
		usleep(1000);
	}
	fclose(salida);
}

int consumidor(void *data)
{
    bool ret = decoder.init();
    if (!ret)
    {
        printf("Error inicializando decodificador\n");
        exit(1);
    }
    FILE *fout = fopen("out.aac", "wb");

    uint32_t size, out_size;
    uint8_t *buffer, *out_buffer;
    int64_t pts, out_pts;
    uint64_t intervalo = 1000000.0 * 1024.0 / 44100.0; // duracion de un frame en usec
	uint64_t captura_siguiente;

	uint64_t espera = 0;
	uint64_t ahora = 0;
	bool primer_frame = true;
    while(!stop) {

        // Mientras hayan datos en el buffer obtenerlos
        while (playoutBuffer.getFrame(&buffer, &size, &pts))
        {
            fwrite(buffer, size, 1, fout);

            // Reproducir el frame
            if (decoder.decode(buffer, size, pts, &out_buffer, &out_size, &out_pts))
            {
            	if(primer_frame) {
            		captura_siguiente = getNow() + intervalo;
            		primer_frame = false;
            	}
                // Frame decodificado! -> Reproducir
                aOut.put(out_buffer, out_size);
            }
            
            // Dormir un tiempo para que el siguiente frame
			// se mande a reproduccion justo cuando se le 
			// necesita.
            ahora = getNow();
		
           
          
	
	if(captura_siguiente > ahora) {
            	espera = captura_siguiente - ahora;
            	usleep(espera);
            }
            
            captura_siguiente += intervalo;
        }
        // Dormir un rato
        //usleep(5); // 1 milisegundo
    }

    fclose(fout);
    decoder.close();
}
