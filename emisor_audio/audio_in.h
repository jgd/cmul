#ifndef __AUDIO_IN_H
#define __AUDIO_IN_H

#include <jack/jack.h>
#include <jack/ringbuffer.h>

#include <SDL/SDL_mutex.h>

class AudioIn {
 public:
  AudioIn();
  ~AudioIn();
  // Devuelve negativo si ha habido un error
  int init();
  int getRate() { return audio_rate; };
  int getChannels() { return channels; };
  int getBytesPerSample() { return bytesPerSample; };
  void close();
  int get(uint8_t **buffer,uint32_t *size);
  int getFramesProcessed() { return frames_processed; };
  int getOverruns() { return overruns; };
  int getSamplesPerFrame() { return nframes; }
 protected:
  jack_client_t *client;
  jack_port_t *input_port1, *input_port2; // Estéreo
  jack_ringbuffer_t *ringbuffer;
  jack_nframes_t nframes;
  uint8_t *outbuffer;
  uint32_t outbuffer_size;
  
  SDL_mutex * thread_lock;
  SDL_cond * data_ready;

  // Bytes de cada muestra
  // Hay dos muestras en cada instante de tiempo, una por canal (estéreo)
  size_t sample_size;
  uint64_t frames_processed;
  uint64_t overruns;
  uint32_t audio_rate;
  uint8_t channels;
  uint8_t bytesPerSample;

  int16_t to_int16(float f);
  static int process_jack(jack_nframes_t nframes, void *arg);
  static void jack_shutdown(void *arg);
};

#endif
