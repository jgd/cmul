#include <signal.h>
#include <unistd.h>
#include "audio_in.h"
#include "audio_out.h"
#include <sndfile.h>
#include <math.h>
#include "aacencoder.h"
#include "aacrtppacketizer.h"

int stop;

void signal_handler(int sig) {
  stop = 1;
}

double log_10(double d) {
  return log(d)/log(10);
}

double calcula_db(const int16_t *buffer, int muestras, int canales) {
  // Calcula la varianza del canal 1º

  double suma_cuadrados = 0.0;
  double suma_simple = 0.0;
  int muestras_canales = muestras*canales;
  for (int i = 0; i < muestras_canales; i+= canales) {
    suma_cuadrados += buffer[i]*buffer[i];
    suma_simple += buffer[i];
  }
  double media_cuadrados = suma_cuadrados / muestras;
  double media_simple = suma_simple / muestras;
  
  return 10 * log_10( media_cuadrados - media_simple * media_simple ) - 10 * log_10 (1 << 30); 
  
}

int main(int argc, const char *argv[]) {

  if (argc != 6) {
    fprintf(stderr,"Uso: %s puertoLocal ipDestino puertoDestino bandwidth payloadType\n",
	    argv[0]);
    exit(1);
  }
  uint16_t puertoLocal = atoi(argv[1]);
  const char* ipDestino = argv[2];
  uint16_t puertoDestino = atoi(argv[3]);
  uint32_t bandwidth = atoi(argv[4]);
  uint8_t payloadType = atoi(argv[5]);

  signal(SIGQUIT, signal_handler);
  signal(SIGTERM, signal_handler);
  signal(SIGHUP, signal_handler);
  signal(SIGINT, signal_handler);

  AudioIn aIn;
  AudioOut aOut;
  SNDFILE *fwav;
  AACEncoder enc;
  stop = 0;
  uint8_t *buffer = 0;
  uint32_t size;
  uint32_t rate;
  uint8_t channels;
  uint8_t bytesPerSample;
  int res = aIn.init();
  if (res < 0) {
    printf("Error inicializando AudioIn\n");
    exit(1);
  }
  res = aOut.init();
  if (res < 0) {
    printf("Error inicializando AudioOut\n");
    exit(1);
  }
  
  rate = aIn.getRate();
  channels = aIn.getChannels();
  bytesPerSample = aIn.getBytesPerSample();
  printf("Audio info: %d Hz, %d channels, %d bytes/sample\n",
	 rate, channels, bytesPerSample);

  SF_INFO sf_info;
  sf_info.samplerate = rate;
  sf_info.channels = channels;
  if (aIn.getBytesPerSample() == bytesPerSample) {
    sf_info.format = SF_FORMAT_WAV|SF_FORMAT_PCM_16;
  }
  else {
    fprintf(stderr,"Error: bytes por muestra no soportado\n");
    exit(1);
  }
  fwav = sf_open("out.wav",SFM_WRITE,&sf_info);
  if (fwav == NULL) {
    fprintf(stderr,"Error abriendo out.wav\n");
    exit(1);
  }
      
  // Configuración del encoder AAC
  if (!enc.init(rate,bandwidth,aIn.getSamplesPerFrame(),channels,
		bytesPerSample*8)) {
    fprintf(stderr,"Error configurando encoder\n");
    exit(1);
  }

  AACRTPPacketizer pack;
  RTPSender rtp;
  // Configuracion del empaquetador AAC/RTP
  if (!pack.init(rate,aIn.getSamplesPerFrame(),&rtp)) {
    fprintf(stderr,"Error configurando empaquetador AAC/RTP\n");
    exit(1);
  }

  // Configuración del emisor RTP
  if (!rtp.init(puertoLocal,ipDestino,puertoDestino,
		rate,bandwidth,payloadType)) {
    fprintf(stderr,"Error configurando emisor RTP\n");
    exit(1);
  }

  int sampleCount;
  int frames_silencio = 0;
  double var;
  uint8_t *frame_comprimido;
  uint32_t frame_comprimido_size;
  int64_t frame_comprimido_pts;

  FILE *fout = fopen("out.aac","wb");

  while (!stop) {
    if (aIn.get(&buffer,&size) < 0) break;
    // Cálculo de la desviación.
    sampleCount = size/(channels * bytesPerSample);
    var = calcula_db((const int16_t*)buffer,sampleCount,channels);
    if (var < -50) {
      frames_silencio++;
    }
    else
      frames_silencio = 0;
    if (frames_silencio > 50) {
      memset(buffer,0,size);
    }
    printf("Var = %g dB (%d muestras)\n", var, sampleCount);
    if (aOut.put(buffer,size) < 0) break;

    sf_writef_short(fwav,(const short *)buffer,sampleCount);

    if (!enc.encode(buffer,&frame_comprimido,&frame_comprimido_size,&frame_comprimido_pts)) {
      fprintf(stderr,"Fallo al comprimir con AAC!\n");
      exit(1);
    }
    if (frame_comprimido_size > 0 && frames_silencio <= 50) {
      fwrite(frame_comprimido,frame_comprimido_size,1,fout);
      pack.send(frame_comprimido,frame_comprimido_size,frame_comprimido_pts);
    }
  }  

  while (1) {
    if (!enc.encode(NULL,&frame_comprimido,&frame_comprimido_size,&frame_comprimido_pts)) {
      fprintf(stderr,"Fallo al comprimir con AAC!\n");
      exit(1);
    }
    if (frame_comprimido_size > 0) {
      fwrite(frame_comprimido,frame_comprimido_size,1,fout);
      pack.send(frame_comprimido,frame_comprimido_size,frame_comprimido_pts);
    }
    else break;
  }

  fclose(fout);
  aIn.close();
  aOut.close();
  enc.close();
  pack.close();
  rtp.close();
  printf("Procesados: %d frames\n",aIn.getFramesProcessed());
  printf("Overruns: %d\n",aIn.getOverruns());

  sf_close(fwav);
}
