#include "aacencoder.h"

AACEncoder::AACEncoder() {
  avcodec_register_all();
  codec = avcodec_find_encoder(AV_CODEC_ID_AAC);
  if (!codec) {
    fprintf(stderr,"No se puede encontrar el compresor AAC!\n");
    exit(1);
  }
  ctx = 0;
  frame = 0;
  frame_buffer = 0;
}

AACEncoder::~AACEncoder() {
  close();
}

bool AACEncoder::init(uint32_t sample_rate, uint32_t bit_rate, uint32_t muestras_por_frame, uint8_t canales, uint8_t bits_por_muestra) {
  ctx = avcodec_alloc_context3(codec);
  if (bits_por_muestra != 16) {
    fprintf(stderr,"Error: solo se soportan 16 bits por muestra!\n");
    return false;
  } 
  ctx->sample_fmt = AV_SAMPLE_FMT_S16;
  ctx->profile = FF_PROFILE_AAC_LOW;
  ctx->sample_rate = sample_rate;
  if (canales != 2) {
    fprintf(stderr,"Error: solo se soportan 2 canales!\n");
    return false;
  }
  ctx->channel_layout = AV_CH_LAYOUT_STEREO;
  ctx->bit_rate = bit_rate;
  ctx->frame_size = muestras_por_frame;
  ctx->time_base = (AVRational){muestras_por_frame,sample_rate};

  if (avcodec_open2(ctx, codec, NULL) < 0) {
    fprintf(stderr, "No se puede abrir el compresor!\n");
    return false;
  }

  // Reserva de memoria
  frame = avcodec_alloc_frame();
  if (!frame) {
    fprintf(stderr, "No se puede reservar un frame!\n");
    return false;
  }
  frame->nb_samples = ctx->frame_size;
  frame->format = ctx->sample_fmt;
  frame->channel_layout = ctx->channel_layout;
  frame_size = av_samples_get_buffer_size(NULL, ctx->channels,
						ctx->frame_size, ctx->sample_fmt, 0);
  frame_buffer = (uint8_t *)av_malloc(frame_size);
  avcodec_fill_audio_frame(frame, ctx->channels, ctx->sample_fmt, frame_buffer, frame_size, 0);

  pts = 0;
  return true;
}

bool AACEncoder::encode(uint8_t *in_buffer, uint8_t **output, uint32_t * out_size, int64_t *out_pts) {
  av_init_packet(&pkt);
  pkt.data = NULL;
  pkt.size = 0;

  if (in_buffer) {
    memcpy(frame_buffer,in_buffer,frame_size);
    frame->pts = pts++;
  }

  int ret,got_output;
  if (in_buffer) ret = avcodec_encode_audio2(ctx, &pkt, frame, &got_output);
  else ret = avcodec_encode_audio2(ctx, &pkt, NULL, &got_output);
  if (ret < 0) {
    fprintf(stderr, "Error codificando frame de audio!\n");
    return false;
  }

  if (got_output) {
    *output = pkt.data;
    *out_size = pkt.size;
    *out_pts = pkt.pts;
  }
  else {
    *output = 0;
    *out_size = 0;
    *out_pts = 0;
  }

  return true;
}

void AACEncoder::free_output() {
  av_free_packet(&pkt);
  pkt.data = NULL;
  pkt.size = 0;
}

void AACEncoder::close() {
  if (ctx) {
    avcodec_close(ctx);
    av_free(ctx);
    ctx = 0;
  }
  if (frame) { 
    avcodec_free_frame(&frame);
    frame = 0;
  }
  if (frame_buffer) {
    av_freep(&frame_buffer);
    frame_buffer = 0;
  }
}
