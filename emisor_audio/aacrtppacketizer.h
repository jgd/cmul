#ifndef __AACRTPPacketizer
#define __AACRTPPacketizer

#include "rtpsender.h"

#define MTU 1472
#define RTPHEADER 12
#define MAXPAYLOAD MTU-RTPHEADER

class AACRTPPacketizer {
 public:
  bool init(uint32_t sample_rate, uint32_t samples_per_frame, RTPSender *sender);
  bool send(uint8_t *buffer, uint32_t size, int64_t pts);
  void close();
  uint32_t getClock();
  uint16_t getMaxPayload();
 private:
  uint32_t sample_rate;
  uint32_t period;
  uint8_t payload[MAXPAYLOAD];
  uint32_t payload_size;
  uint8_t headers[MAXPAYLOAD];
  uint32_t headers_size;
  uint8_t aus[MAXPAYLOAD];
  uint32_t aus_size;
  uint8_t frames;
  RTPSender* sender;
  int64_t current_pts;
  bool first_packet;
};
#endif 
