#ifndef __AACEncoder
#define __AACEncoder

// FFMPEG
extern "C" {
#include <libavcodec/avcodec.h>

}

class AACEncoder {
 public:
  AACEncoder();
  ~AACEncoder();
  bool init(uint32_t sample_rate, uint32_t bit_rate, uint32_t muestras_por_frame, uint8_t canales, uint8_t bits_por_muestra);
  bool encode(uint8_t *in_buffer, uint8_t **output, uint32_t * out_size, int64_t *out_pts);
  void free_output();
  void close();
 private:
  AVCodec *codec;
  AVCodecContext *ctx;
  AVFrame *frame;
  AVPacket pkt;
  int64_t pts;
  uint8_t *frame_buffer;
  uint32_t frame_size;
};

#endif
