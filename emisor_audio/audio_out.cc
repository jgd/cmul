#include "audio_out.h"

AudioOut::AudioOut() {
  client = 0;
  ringbuffer = 0;
  thread_lock = 0;
}

AudioOut::~AudioOut() {
  close();
}

float AudioOut::to_float(int16_t i) {
  if (i > 0x7fff) i = 0x7fff;
  if (i < -0x7fff) i = -0x7fff;
  return ((float)i / 0x7fff);
}

int AudioOut::process_jack(jack_nframes_t nframes, void *arg) {
  AudioOut* aOut = (AudioOut*)arg; 
  jack_default_audio_sample_t *out1, *out2;
  int i,size,sizeS;
  float f;
  int16_t s;
 
  out1 = (jack_default_audio_sample_t *)jack_port_get_buffer (aOut->output_port1, nframes);
  out2 = (jack_default_audio_sample_t *)jack_port_get_buffer (aOut->output_port2, nframes);
   
  // Copia del buffer circular al buffer de los puertos de salida
  SDL_mutexP(aOut->thread_lock);
  // Tamaño en bytes
  size = jack_ringbuffer_read_space(aOut->ringbuffer);
  // Tamaño en muestras para ambos canales.
  sizeS = size / 2 / 2; 
  if (sizeS >= nframes) {
    for (i = 0; i < nframes; i++) {
      jack_ringbuffer_read(aOut->ringbuffer, (char*)&s, 2);
      out1[i] = aOut->to_float(s);
      jack_ringbuffer_read(aOut->ringbuffer, (char*)&s, 2);
      out2[i] = aOut->to_float(s);
    }
  }
  SDL_mutexV(aOut->thread_lock);
  (aOut->frames_processed)++;
  
  return 0;
}

void AudioOut::jack_shutdown(void *arg) {
  fprintf(stderr, "JACK shut down, exiting...\n");
  exit(1);
}

// Inicializa Jack
int AudioOut::init() {
  frames_processed = 0;
  channels = 2;
  bytesPerSample = 2;

  client = jack_client_open("audioOut-umu", JackNullOption, NULL);
  if (client == 0) {
    fprintf(stderr, "JACK server not running?\n");
    return -1;
  }

  audio_rate = jack_get_sample_rate(client);

  jack_set_process_callback(client, process_jack, this);
  jack_on_shutdown(client, jack_shutdown, 0);

  // Salida canal izquierdo
  output_port1 = jack_port_register (client, "output1",
				    JACK_DEFAULT_AUDIO_TYPE,
				    JackPortIsOutput, 0);
  // Salida canal derecho
  output_port2 = jack_port_register (client, "output2",
				    JACK_DEFAULT_AUDIO_TYPE,
				    JackPortIsOutput, 0);

  if ( (output_port1 == NULL) || (output_port2 == NULL) ) {
    fprintf(stderr, "no more output JACK ports available\n");
    return -1;
  }

  // Recupera la cantidad de muestras que se pasarán al método process
  nframes = jack_get_buffer_size(client);
  // Recupera el tamaño de las muestras
  sample_size = sizeof(jack_default_audio_sample_t);
 
  // Crea un buffer circular para guardar los datos con muestras de formato s16
  // 2 = stereo, 2 = s16, 8 = frames
  ringbuffer = jack_ringbuffer_create(channels * 2 * 8 * nframes); 
  memset(ringbuffer->buf, 0, ringbuffer->size);

  // El cliente jack se activa antes de conectar los puertos!
  if (jack_activate(client)) {
    fprintf(stderr, "Cannot activate jack client!\n");
    exit(1);
  }

  // Conecta los puertos de la aplicacion con los de la tarjeta
  const char **ports;

  ports = jack_get_ports (client, NULL, NULL, JackPortIsPhysical|JackPortIsInput);
  if (ports == NULL) {
    fprintf(stderr, "no physical capture ports\n");
    return -1;
  }

  if (jack_connect (client, jack_port_name (output_port1), ports[0])) {
    fprintf (stderr, "cannot connect output port 1\n");
  }
		
  if (jack_connect (client, jack_port_name (output_port2), ports[1])) {
    fprintf (stderr, "cannot connect output port 2\n");
  }
  free (ports);

  thread_lock = SDL_CreateMutex();

  return 0;
}

void AudioOut::close() {
  if (client) jack_client_close(client);
  if (ringbuffer) jack_ringbuffer_free(ringbuffer);
  if (thread_lock) SDL_DestroyMutex(thread_lock);
  client = 0;
  ringbuffer = 0;
  thread_lock = 0;
}

// Devuelve 0 si ha ido bien
// -1 si ha habido error.
int AudioOut::put(uint8_t *buffer, uint32_t size) {
  int ret = 0;
  SDL_mutexP(thread_lock);
  if (jack_ringbuffer_write(ringbuffer, (const char *)buffer, size) < size) {
    fprintf(stderr,"Output ringbuffer overrrun!\n");
    ret = -1;
  }
  SDL_mutexV(thread_lock);
  return ret;
}
