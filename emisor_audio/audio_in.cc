#include "audio_in.h"

AudioIn::AudioIn() {
  client = 0;
  ringbuffer = 0;
  outbuffer = 0;
  thread_lock = 0;
  data_ready = 0;
}

AudioIn::~AudioIn() {
  close();
}

int16_t AudioIn::to_int16(float f) {
  if (f > 1.0) f = 1.0;
  if (f < -1.0) f = -1.0;
  return (int16_t)(f* 0x7fff);
}

int AudioIn::process_jack(jack_nframes_t nframes, void *arg) {
  AudioIn* aIn = (AudioIn*)arg; 
  jack_default_audio_sample_t *in1, *in2;
  int i;
  int16_t s;
 
  in1 = (jack_default_audio_sample_t *)jack_port_get_buffer (aIn->input_port1, nframes);
  in2 = (jack_default_audio_sample_t *)jack_port_get_buffer (aIn->input_port2, nframes);
  
  // Copia al buffer circular la información de entrada en formato s16
  SDL_mutexP(aIn->thread_lock);
  for (i = 0; i < nframes; i++) {
    s = aIn->to_int16(in1[i]);
    if (jack_ringbuffer_write(aIn->ringbuffer, (const char *)&s, 2) < 2) (aIn->overruns)++;
    s = aIn->to_int16(in2[i]);
    if (jack_ringbuffer_write(aIn->ringbuffer, (const char *)&s, 2) < 2) (aIn->overruns)++;
  } 
  SDL_mutexV(aIn->thread_lock);
  // Hay datos: informar al thread consumidor
  SDL_CondSignal(aIn->data_ready);
  (aIn->frames_processed)++;
  
  return 0;
}

void AudioIn::jack_shutdown(void *arg) {
  fprintf(stderr, "JACK shut down, exiting...\n");
  exit(1);
}

// Inicializa Jack
int AudioIn::init() {
  frames_processed = 0;
  overruns = 0;
  
  channels = 2;
  bytesPerSample = 2;

  client = jack_client_open("audioIn-umu", JackNullOption, NULL);
  if (client == 0) {
    fprintf(stderr, "JACK server not running?\n");
    return -1;
  }

  audio_rate = jack_get_sample_rate(client);

  jack_set_process_callback(client, process_jack, this);
  jack_on_shutdown(client, jack_shutdown, 0);

  // Entrada canal izquierdo
  input_port1 = jack_port_register (client, "input1",
				    JACK_DEFAULT_AUDIO_TYPE,
				    JackPortIsInput, 0);
  // Entrada canal derecho
  input_port2 = jack_port_register (client, "input2",
				    JACK_DEFAULT_AUDIO_TYPE,
				    JackPortIsInput, 0);

  if ( (input_port1 == NULL) || (input_port2 == NULL) ) {
    fprintf(stderr, "no more input JACK ports available\n");
    return -1;
  }

  // Recupera la cantidad de muestras que se pasarán al método process
  nframes = jack_get_buffer_size(client);
  // Recupera el tamaño de las muestras
  sample_size = sizeof(jack_default_audio_sample_t);
 
  // Crea un buffer circular para guardar los datos con muestras de formato s16
  // 2 = stereo, 2 = s16, 8 = frames
  ringbuffer = jack_ringbuffer_create(channels * 2 * 8 * nframes); 
  memset(ringbuffer->buf, 0, ringbuffer->size);

  // El cliente jack se activa antes de conectar los puertos!
  if (jack_activate(client)) {
    fprintf(stderr, "Cannot activate jack client!\n");
    exit(1);
  }

  // Conecta los puertos de la aplicacion con los de la tarjeta
  const char **ports;

  ports = jack_get_ports (client, NULL, NULL, JackPortIsPhysical|JackPortIsOutput);
  if (ports == NULL) {
    fprintf(stderr, "no physical capture ports\n");
    return -1;
  }

  if (jack_connect (client, ports[0], jack_port_name (input_port1))) {
    fprintf (stderr, "cannot connect input port 1\n");
  }
		
  if (jack_connect (client, ports[1], jack_port_name (input_port2))) {
    fprintf (stderr, "cannot connect input port 2\n");
  }
  free (ports);

  thread_lock = SDL_CreateMutex();
  data_ready = SDL_CreateCond();

  outbuffer_size = nframes * bytesPerSample * channels;
  outbuffer = new uint8_t[outbuffer_size];

  return 0;
}

void AudioIn::close() {
  if (client) jack_client_close(client);
  if (ringbuffer) jack_ringbuffer_free(ringbuffer);
  if (outbuffer) delete [] outbuffer;
  if (thread_lock) SDL_DestroyMutex(thread_lock);
  if (data_ready) SDL_DestroyCond(data_ready);
  client = 0;
  ringbuffer = 0;
  outbuffer = 0;
  thread_lock = 0;
  data_ready = 0;
}

// Devuelve 0 si ha ido bien
// -1 si ha habido error.
int AudioIn::get(uint8_t **buffer, uint32_t *size) {
  SDL_mutexP(thread_lock);
  int ret = SDL_CondWait(data_ready,thread_lock); // Espera datos
  if (ret == 0) {
    // Hay datos
    if (jack_ringbuffer_read_space(ringbuffer) >= outbuffer_size) { 
      *size = outbuffer_size;
      *buffer = outbuffer;
      jack_ringbuffer_read(ringbuffer, (char*)outbuffer, outbuffer_size);
    }
  }
  SDL_mutexV(thread_lock);
  return ret;
}
