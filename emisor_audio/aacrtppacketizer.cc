#include "aacrtppacketizer.h"

bool AACRTPPacketizer::init(uint32_t sample_rate, uint32_t samples_per_frame, RTPSender *sender) {
  if (!sender) return false;
  this->sample_rate = sample_rate;
  this->period = samples_per_frame;
  this->sender = sender;
  payload_size = 0;
  frames = 0;
  current_pts = 0;
  headers_size = 2;
  aus_size = 0;
  first_packet = true;
  return true;
}

bool AACRTPPacketizer::send(uint8_t *buffer, uint32_t size, int64_t pts) {
  bool res = true;
  if (size > getMaxPayload()) return false;
  if (first_packet) {
    current_pts = pts;
    first_packet = false;
  }
  if (headers_size + aus_size + size + 2 <= MAXPAYLOAD) {
    // El nuevo frame AAC cabe en el paquete
    uint16_t aux_size = (uint16_t)size;
    headers[headers_size] = (aux_size >> 5) & 0xFF;
    headers[headers_size+1] = (aux_size & 0x1F) << 3;
    headers_size += 2;
    memcpy(&(aus[aus_size]),buffer,size);
    aus_size += size;
  } 
  else {
    // El nuevo frame no cabe. Envio el paquete
    uint16_t headers_length = htons((headers_size-2)*8);
    memcpy(&(headers[0]),&headers_length,2);
    memcpy(payload,headers,headers_size);
    payload_size = headers_size;
    memcpy(&(payload[payload_size]),aus,aus_size);
    payload_size += aus_size;
    res = sender->send(payload,payload_size,current_pts*period,true);
    
    // El nuevo frame pasa a ocupar el comienzo de los buffers headers y aus
    uint16_t aux_size = (uint16_t)size;
    headers_size = 2;
    headers[headers_size] = (aux_size >> 5) & 0xFF;
    headers[headers_size+1] = (aux_size & 0x1F) << 3;
    headers_size += 2;
    aus_size = 0;
    memcpy(&(aus[aus_size]),buffer,size);
    aus_size += size;
    current_pts = pts;
  }
  return res;
}

void AACRTPPacketizer::close() {
}
 
uint32_t AACRTPPacketizer::getClock() {
	return sample_rate;
}

uint16_t AACRTPPacketizer::getMaxPayload() {
	return MAXPAYLOAD - 4;
}











