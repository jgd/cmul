#ifndef __AUDIO_OUT_H
#define __AUDIO_OUT_H

#include <jack/jack.h>
#include <jack/ringbuffer.h>

#include <SDL/SDL_mutex.h>

class AudioOut {
 public:
  AudioOut();
  ~AudioOut();
  // Devuelve negativo si ha habido un error
  int init();
  int getRate() { return audio_rate; };
  int getChannels() { return channels; };
  int getBytesPerSample() { return bytesPerSample; };
  void close();
  int put(uint8_t *buffer,uint32_t size);
  int getFramesProcessed() { return frames_processed; };
 protected:
  jack_client_t *client;
  jack_port_t *output_port1, *output_port2; // Estéreo
  jack_ringbuffer_t *ringbuffer;
  jack_nframes_t nframes;
  
  SDL_mutex * thread_lock;

  // Bytes de cada muestra
  // Hay dos muestras en cada instante de tiempo, una por canal (estéreo)
  size_t sample_size;
  uint64_t frames_processed;
  uint32_t audio_rate;
  uint8_t channels;
  uint8_t bytesPerSample;

  float to_float(int16_t i);
  static int process_jack(jack_nframes_t nframes, void *arg);
  static void jack_shutdown(void *arg);
};

#endif
