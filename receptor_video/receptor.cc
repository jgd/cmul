#include "rtpreceiver.h"
#include "h264rtpdepacketizer.h"
#include "h264decoder.h"
#include "display.h"
#include <signal.h>
#include <stdio.h>
#include <unistd.h>

#define INFOHEIGHT 50

bool stop;
RTPReceiver receiver;
H264RTPDepacketizer depack;
H264Decoder decoder;

void sighand(int sig) {
  stop = true;
  depack.parar();
}

int main(int argc, char *argv[]) {
  if (argc != 3) {
    fprintf(stderr, "Uso: %s puerto fps\n", argv[0]);
    exit(1);
  }

  bool ret = receiver.init(atoi(argv[1]),depack.getClock());
  if (!ret) exit(1);

  ret = depack.init(atoi(argv[2]),&receiver);
  if (!ret) exit(1);

  ret = decoder.init();
  if (!ret) exit(1);

  stop = false;
  signal(SIGINT, sighand);
  signal(SIGQUIT, sighand);

  //FILE *fout = fopen("out.yuv","wb");

  uint8_t *buffer;
  uint32_t size;
  int64_t in_pts;
  FrameYUV *f = NULL;
  int64_t out_pts;

  DisplayYUV *display = NULL;
  Rect rOverlay;
  OverlayYUV *overlay = NULL;

  while(!stop) {  
    if (depack.receive(&buffer,&size,&in_pts)) {
      // Decodifico la Unidad de Acceso
      if (decoder.decode(buffer,size,in_pts,&f,&out_pts)) {
	// Frame decodificado
	if (display == NULL) {	  
	  display = DisplayYUV::init(f->width,f->height+INFOHEIGHT);  
	  rOverlay.x = 0;
	  rOverlay.y = 0;
	  rOverlay.w = f->width;
	  rOverlay.h = f->height;
	  overlay = new OverlayYUV(display, &rOverlay);
	  display->caption((char*)"Comunicaciones Multimedia");
	}
	// Muestro el frame
	overlay->display(f);
	//fwrite(f->data,1,f->size,fout);
      }
      free(buffer);
      if (display && display->poll_quit()) break;
    }
    else break;
  }
  //fclose(fout);
  receiver.close();
  depack.close();
  decoder.close();
  DisplayYUV::close();
  if (overlay) delete overlay;
  if (f) delete f;
}
