#include "h264rtpdepacketizer.h"
#include <iostream>
#include <unistd.h>

using namespace std;

bool H264RTPDepacketizer::init(uint16_t fps, RTPReceiver *receiver, uint32_t firstTS) {
  if (!receiver || !fps) return false;
  period = RTPCLOCK/fps;
  this->receiver = receiver;
  this->firstTS = firstTS;
  seqNum = 0;
  first_packet = true;
  return true;
}

bool H264RTPDepacketizer::receive(uint8_t **buffer, uint32_t *size, int64_t *pts) {
  uint8_t *au = NULL;
  uint32_t au_size = 0;
  uint8_t prefix[4] = {0x00,0x00,0x00,0x01};

  stop = false;
  while (!stop) {
    if (!receiver->getPayloadData()) {
      if (receiver->isClosed()) {
	if (au_size > 0) {
	  // Final de la recepcion
	  // Flush de lo que quede!
	  *buffer = au;
	  *size = au_size;
	  return true;
	}
	else return false;
      }
      if (!receiver->receive()) {
	usleep(1); // Espera el minimo antes de volver a intentarlo
      }
      else {
       	if (first_packet) 
	  seqNum = receiver->getSeqNum();
	else {
	  if (receiver->getSeqNum() != seqNum + 1)
	    cerr << "Detectada perdida: esperado "<< seqNum + 1 << " y recibido " << receiver->getSeqNum() << endl;
	  seqNum = receiver->getSeqNum();
	}
      }
    }
    else {
      // Hay datos
      if (first_packet) {
	first_packet = false;
	timestamp = receiver->getTimestamp();
	if (firstTS == ~0) firstTS = timestamp;
      }
      if (timestamp == receiver->getTimestamp()) {
	// El paquete forma parte del mismo Access Unit
	const uint8_t *payload = receiver->getPayloadData();
	uint32_t payload_size = receiver->getPayloadSize();
	if ((payload[0] & 0x1f) < 24) {
	  // Una sola NALU en el paquete
	  // Ampliar el buffer para hacerle sito al final
	  au = (uint8_t *)realloc(au, au_size + 4 + payload_size);
	  if (!au) {
	    cerr << "Error reservando memoria\n" << endl; exit(1);
	  }
	  // Copiar datos
	  memcpy(&(au[au_size]),prefix,4);
	  au_size += 4;
	  memcpy(&(au[au_size]),payload,payload_size);
	  au_size += payload_size;
	} 
	else if ((payload[0] & 0x1f) == 24) {
	  // STAP 
	  // Recorrer todas las NALU del paquete
	  uint32_t pos = 1;
	  uint16_t nalu_size = 0;
	  
	  while (pos < payload_size) {
	    memcpy(&nalu_size,&(payload[pos]),2);
	    nalu_size = ntohs(nalu_size);
	    pos += 2;
	    // Ampliar el buffer para hacerle sitio a la nueva NALU
	    au = (uint8_t *)realloc(au, au_size + 4 + nalu_size);
	    if (!au) {
	      cerr << "Error reservando memoria\n" << endl; exit(1);
	    }
	    memcpy(&(au[au_size]),prefix,4);
	    au_size += 4;
	    memcpy(&(au[au_size]),&(payload[pos]),nalu_size);
	    au_size += nalu_size;
	    pos += nalu_size;
	  }
	} 
	receiver->freePacket();
      }
      else {
      // El paquete ya no pertenece al mismo Access Unit
	*buffer = au;
	*size = au_size;
	uint32_t diff = timestamp - firstTS;
	*pts = diff/period;
	// Actualizo el timestamp para el siguiente Access Unit
	timestamp = receiver->getTimestamp();
	return true;	
      }
    }
  }
  return false;
}
