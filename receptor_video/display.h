#ifndef __DISPLAY
#define __DISPLAY

#include "frame.h"
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

class Display {
 public:            
  int caption(char *label);
  virtual int display(Frame *f) = 0;
  bool poll_quit();
  unsigned long now();
  void print(int16_t x, int16_t y, char *str, uint8_t r = 255, uint8_t g = 255, uint8_t b = 255);
  
 protected:      
  Display(int width, int height, int *ret);       
  ~Display();
  SDL_Surface *screen;
  int width;
  int height;
  SDL_Event sdl_event;
  TTF_Font *font;
};

struct Rect {
  int x,y,w,h;
};

class OverlayYUV;

class DisplayYUV : public Display {
  friend class OverlayYUV;
 public:
  static DisplayYUV *init(int width, int height);
  static void close();
  int display(Frame *f);      
 protected:
  static DisplayYUV *instancia;
  DisplayYUV(int width, int height, int *ret);
  ~DisplayYUV();
  OverlayYUV *overlay;
};

class OverlayYUV {
 public:
  OverlayYUV(DisplayYUV *d, Rect *r);
  ~OverlayYUV();
  int display(Frame *f);
 protected:
  SDL_Overlay *overlay;
  SDL_Rect rect;
};

#endif
