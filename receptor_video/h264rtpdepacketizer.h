#ifndef __H264RTPDepacketizer
#define __H264RTPDepacketizer

#include "rtpreceiver.h"
#define RTPCLOCK 90000

class H264RTPDepacketizer {
 public:
  bool init(uint16_t fps, RTPReceiver *receiver, uint32_t firstTS = ~0);
  bool receive(uint8_t **buffer, uint32_t *size, int64_t *pts);
  void close() { parar(); }
  void parar() { stop = true; }
  uint32_t getClock() { return RTPCLOCK; }
 private:
  uint32_t period;
  RTPReceiver * receiver;
  uint32_t timestamp, firstTS;
  uint32_t seqNum;
  bool stop;
  bool first_packet;
};

#endif
