#include "display.h"
#include <string.h>

Display::Display(int _width, int _height, int *ret) {
  *ret = 0;
  width = _width;
  height = _height;    
  screen = SDL_SetVideoMode(width, height, 24, SDL_SWSURFACE);  
  if (screen == 0) {
    fprintf(stderr,"No se pudo configurar el modo de video\n");
    *ret = -1; 
  }
  font = TTF_OpenFont("./Ubuntu-M.ttf",14);
  if (font == 0) {
    fprintf(stderr,"No se pudo cargar el tipo de letra: %s\n",TTF_GetError());
    *ret = -1;
  }
}

int Display::caption(char *label) {
  SDL_WM_SetCaption (label, NULL);  
  return 0;
}

Display::~Display() {
  TTF_CloseFont(font);
  TTF_Quit();
  SDL_Quit();
}

bool Display::poll_quit() {
  SDL_PollEvent(&sdl_event);
  if (sdl_event.type == SDL_QUIT) return true;
  else return false;
}

unsigned long Display::now() {
  return SDL_GetTicks()*1000; // en microsegundos
}

void Display::print(int16_t x, int16_t y, char *str, uint8_t r, uint8_t g, uint8_t b) {
  SDL_Color textFGcolor;
  textFGcolor.r = r;
  textFGcolor.g = g;
  textFGcolor.b = b;
  SDL_Surface *text = TTF_RenderText_Blended(font,str,textFGcolor);
  SDL_Rect rect;
  rect.x = x;
  rect.y = y;
  rect.w = text->w;
  rect.h = text->h;
  SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, 0, 0, 0));
  if (SDL_BlitSurface(text,NULL,screen,&rect) < 0) {
    fprintf(stderr,"Error display::print: %s\n",SDL_GetError());
  }
  SDL_FreeSurface(text);
  SDL_Flip(screen);
}

DisplayYUV *DisplayYUV::instancia = 0;

DisplayYUV::DisplayYUV(int width, int height, int *ret) : Display(width, height, ret) {
  if (*ret != 0) return;
  // Creo overlay sobre toda la ventana
  Rect rect;
  rect.x = 0;
  rect.y = 0;
  rect.w = width;
  rect.h = height;
  overlay = new OverlayYUV(this, &rect); 
  if (overlay == 0) {
    fprintf(stderr, "No se puede crear overlay YUYV: %s\n",SDL_GetError());
    *ret -1;  
  }
}

DisplayYUV::~DisplayYUV() {
  delete overlay;
}

DisplayYUV *DisplayYUV::init(int width, int height) {
  if (instancia == 0) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
      fprintf(stderr, "No se puede inicializar SDL: %s\n",SDL_GetError());    
    else if (TTF_Init() < 0) 
      fprintf(stderr, "No se puede inicializar TTF: %s\n",TTF_GetError());
    else {
      atexit(SDL_Quit);  
      atexit(TTF_Quit);
      int res = 0;
      instancia = new DisplayYUV(width, height, &res); 
      if (res<0) {       
	delete instancia;
	instancia = 0;
      }
    }
  }

  return instancia;
}

void DisplayYUV::close() {   
  delete instancia;
  instancia = 0;
}

int DisplayYUV::display(Frame *f) {
  return overlay->display(f);
}

OverlayYUV::OverlayYUV(DisplayYUV *d, Rect *_r) { 
  overlay = SDL_CreateYUVOverlay(_r->w, _r->h, SDL_IYUV_OVERLAY, d->screen);
  rect.x = _r->x;
  rect.y = _r->y;
  rect.w = (unsigned int)_r->w;
  rect.h = (unsigned int)_r->h;
}

OverlayYUV::~OverlayYUV() {
  SDL_FreeYUVOverlay(overlay);
}

int OverlayYUV::display(Frame *f) {
  FrameYUV *fyuv = (FrameYUV *)f;
  SDL_LockYUVOverlay(overlay);  
  memcpy(overlay->pixels[0],fyuv->Y(),fyuv->sizeY);
  memcpy(overlay->pixels[1],fyuv->U(),fyuv->sizeUV);
  memcpy(overlay->pixels[2],fyuv->V(),fyuv->sizeUV);
  SDL_UnlockYUVOverlay(overlay);
  SDL_DisplayYUVOverlay(overlay,&rect);
  return 0;
}

